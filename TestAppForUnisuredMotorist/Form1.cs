﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TestAppForUnisuredMotorist.CopQueryService;

namespace TestAppForUnisuredMotorist
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            try
            {
                using (SoapUninsuredMotoristServiceClient proxy = new SoapUninsuredMotoristServiceClient())
                {
                    BasicLookupRequest request = new BasicLookupRequest();
                    if (txtDL.Text == "")
                    {
                        request.LicensePlate = txtPlate.Text;
                        request.VIN = txtVIN.Text;

                    }
                    else
                    {
                        request.DriversLicenseNumber = txtDL.Text;
                    }
                    BasicLookupResponse response = proxy.BasicLookupByType(request);

                    txtInsInfo.Text = response.InsuranceInfo;

                }
            }
            catch (FaultException<CopQueryService.ResultEnt> err)
            {
                ShowErrorMessage(err.Detail.Message);
                
            }
            catch (Exception err)
            {


                ShowErrorMessage(err.Message);
            }

        }

        private static void ShowErrorMessage(string errMessage)
        {
            MessageBox.Show(errMessage);
            
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtDL.Text = "";
            txtInsInfo.Text = "";
        }
    }
}
