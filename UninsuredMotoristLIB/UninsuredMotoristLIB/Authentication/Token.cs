﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UninsuredMotoristLIB.Resource.Ent;

namespace UninsuredMotoristLIB.Authentication
{
    public class Token
    {
  
        public static string BuildToken(string clientName)
        {
            string tokenString = "";

            if (clientName != "")
            {

                string currentYear = Convert.ToString(DateTime.Today.Year);
                string currentMonth = Convert.ToString(DateTime.Today.Month);
                string currentDay = Convert.ToString(DateTime.Today.Day);
                string currentHour = DateTime.Now.TimeOfDay.Hours.ToString(); // ???????
                string currentMinute = DateTime.Now.TimeOfDay.Minutes.ToString();



                string currentTime = currentHour + ":" + currentMinute;
                string currentDate = currentMonth + "/" + currentDay + "/" + currentYear;

                tokenString = clientName + "|" + currentDate + "|" + currentTime;

                /* Encrypt the token to send back  */

                //tokenString = Encrypt.EncryptString(tokenString);
            }

            return tokenString;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="clientToken"></param>
        /// <returns></returns>
        internal TokenEntity SplitToken(string clientToken)
        {
            /* Decrypt the token to populate the entity  */

            clientToken = Decrypt.DecryptString(clientToken);
            char delimiter = Convert.ToChar("|");
            string[] parts = clientToken.Split(delimiter);

            TokenEntity tok = new TokenEntity();
            tok.ClientName = parts[0];
            tok.TokenDate = DateTime.Parse(parts[1]);
            tok.TokenTime = DateTime.Parse(parts[2]);

            return tok;
        }

        internal static string SplitTokenClient(string clientToken)
        {
            /* Decrypt the token to populate the entity  */

            clientToken = Decrypt.DecryptString(clientToken);
            char delimiter = Convert.ToChar("|");
            string[] parts = clientToken.Split(delimiter);

            return parts[0];

        }

        internal static string SplitTokenDateTime(string clientToken)
        {
            /* Decrypt the token to populate the entity  */

            clientToken = Decrypt.DecryptString(clientToken);
            char delimiter = Convert.ToChar("|");
            string[] parts = clientToken.Split(delimiter);


            return parts[1] + " " + parts[2];

        }

        //public static DateTime SplitTokenTime(string clientToken)
        //{
        //    /* Decrypt the token to populate the entity  */

        //    clientToken = Decrypt.DecryptString(clientToken);
        //    char delimiter = Convert.ToChar("|");
        //    string[] parts = clientToken.Split(delimiter);


        //    return DateTime.Parse(parts[2]);

        //}
    }
}
