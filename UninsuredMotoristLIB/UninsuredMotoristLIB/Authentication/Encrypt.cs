﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace UninsuredMotoristLIB.Authentication
{
    public static class Encrypt
    {

        public static string EncryptString(string DataToEncrypt)
        {

            try
            {
                byte[] encData_byte = new byte[DataToEncrypt.Length];
                encData_byte = System.Text.Encoding.UTF8.GetBytes(DataToEncrypt);
                string encodedData = Convert.ToBase64String(encData_byte);
                return encodedData;
            }
            catch (Exception e)
            {
                throw new Exception("Error in Encode" + e.Message);
            }
        }


        /// <summary>
        /// Stubbed out MD5 string
        /// </summary>
        /// <param name="DataToEncrypt"></param>
        /// <returns></returns>
        public static string EncryptStringMD5(string DataToEncrypt)
        {
            try
            {
                string encodedData = HashMD5(DataToEncrypt);
                return encodedData;
            }
            catch (Exception e)
            {
                throw new Exception("Error in Encode - EncryptStringMD5 " + e.Message);
            }
        }

        /// <summary>
        /// Hash Routine
        /// </summary>
        /// <param name="ToHash"></param>
        /// <returns></returns>
        private static string HashMD5(string ToHash)
        {
            try
            {
                // First we need to convert the string into bytes, which means using a text encoder.
                Encoder enc = System.Text.Encoding.ASCII.GetEncoder();

                // Create a buffer large enough to hold the string
                byte[] data = new byte[ToHash.Length];
                enc.GetBytes(ToHash.ToCharArray(), 0, ToHash.Length, data, 0, true);

                // This is one implementation of the abstract class MD5.
                MD5 md5 = new MD5CryptoServiceProvider();
                byte[] result = md5.ComputeHash(data);

                return BitConverter.ToString(result).Replace("-", "").ToLower();
            }
            catch (Exception e)
            {

                throw new Exception("Error in Encode - Hash " + e.Message); ;
            }
        }
    }
}
