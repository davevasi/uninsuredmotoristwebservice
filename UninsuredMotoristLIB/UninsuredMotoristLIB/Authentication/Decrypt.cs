﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace UninsuredMotoristLIB.Authentication
{
    public class Decrypt
    {
        public static string DecryptString(string DataToDecrypt)
        {
            try
            {
                System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
                System.Text.Decoder utf8Decode = encoder.GetDecoder();

                byte[] todecode_byte = Convert.FromBase64String(DataToDecrypt);
                int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
                char[] decoded_char = new char[charCount];
                utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
                string result = new String(decoded_char);
                return result;
            }
            catch (Exception e)
            {
                throw new Exception("Error in Decode" + e.Message);
            }
        }
    }
}
