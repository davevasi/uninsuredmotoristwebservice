﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace UninsuredMotoristLIB.Authentication
{
    public class Validate
    {
        //Decrpyt the token
        //Validate the Date and Time is Today and within the last ten minutes
        /// <summary>
        /// 
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public static bool ValidateToken(string token)
        {
            string clientName = Token.SplitTokenClient(token);
            string tokDateTime = Token.SplitTokenDateTime(token);
            //DateTime tokTime = Token.SplitTokenTime(token);


            /* Read the initial time. */
            DateTime startTime = DateTime.Parse(DateTime.Now.ToShortTimeString());


            /* Read the end time. */
            DateTime stopTime = DateTime.Parse(tokDateTime);


            /* Compute the duration between the initial and the end time. */
            TimeSpan duration = startTime - stopTime;

            if (duration.Minutes < 10)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
    }
}
