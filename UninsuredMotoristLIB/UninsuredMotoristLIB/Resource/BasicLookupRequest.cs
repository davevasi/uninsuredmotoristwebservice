﻿using System;

namespace UninsuredMotoristLIB.Resource
{
    public class BasicLookupRequest : RequestBase
    {

   
        public string DriversLicenseNumber { get; set; }

        public string VIN { get; set; }

        public string LicensePlate { get; set; }

        public int IpAddress { get; set; }

        public int IPPort { get; set; }

        public string ServerName { get; set; }





    }
}
