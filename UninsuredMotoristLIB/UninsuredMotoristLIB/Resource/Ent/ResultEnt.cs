﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using UninsuredMotoristLIB.Common.Enums;

namespace UninsuredMotoristLIB.Resource.Ent
{   [DataContract]
    public class ResultEnt
    {

        private ResultType codeField;


        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>The code.</value>
        /// 
        [DataMember]
        public ResultType ErrorCode
        {
            get { return codeField; }
            set { codeField = value; }
        }


        private string messageField;

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>The message.</value>
        /// 
        [DataMember]
        public string Message
        {
            get { return messageField; }
            set { messageField = value; }
        }

     

    }
}
