﻿using System;
using System.Collections.Generic;

using System.Text;
using System.Threading.Tasks;

namespace UninsuredMotoristLIB.Resource.Ent
{
    public class TokenEntity
    {
        private string _clientName;
        private DateTime _tokenDate;
        private DateTime _tokenTime;

        /// <summary>
        /// 
        /// </summary>
        public DateTime TokenTime
        {
            get { return _tokenTime; }
            set { _tokenTime = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime TokenDate
        {
            get { return _tokenDate; }
            set { _tokenDate = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ClientName
        {
            get { return _clientName; }
            set { _clientName = value; }
        }

    }
}
