﻿
using UninsuredMotoristLIB.Resource.Ent;

namespace UninsuredMotoristLIB
{
    public abstract class ResponseBase
    {

        private string sessionToken;
        private ResultEnt _result;


        public ResultEnt Result
        {
            get { return _result; }
            set { _result = value; }
        }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string SessionToken
        {
            get { return sessionToken; }
            set { sessionToken = Authentication.Encrypt.EncryptString(value); }
        }
    }
}
