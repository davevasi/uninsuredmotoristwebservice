﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Linq;
using UninsuredMotoristLIB.WF;
using UninsuredMotoristLIB.Resource;
using UninsuredMotoristLIB.Resource.Ent;
using UninsuredMotoristLIB.Common.Enums;
using System.Activities;
using UninsuredMotoristLIB.Common;


namespace UninsuredMotoristLIB
{
    public abstract class UninsuredMotoristServiceBase
    {

        #region Public Methods


        public BasicLookupResponse BasicLookupByType(BasicLookupRequest request)
        {
            try
            {
                return basicLookupByType(request);
            }
            catch (Exception err)
            {
                ResultEnt error = new ResultEnt();
                error.ErrorCode = ResultType.Error;
                error.Message = err.Message;
              throw new FaultException<ResultEnt>(error,new FaultReason(error.Message),new FaultCode(error.ErrorCode.ToString()));                
            }

        }


        #endregion



        #region Private Methods

        private BasicLookupResponse basicLookupByType(BasicLookupRequest request)
        {

            try
            {
                OperationContext context = OperationContext.Current;
                MessageProperties messageProperties = context.IncomingMessageProperties;
                RemoteEndpointMessageProperty endpointProperty = messageProperties[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;


                string ipAddress = endpointProperty.Address;
                int ipPort = endpointProperty.Port;
                string serverName = System.Environment.MachineName;


                using (var dc = new CopQueryDataContext())
                {
                    var qry = (from r in dc.Clients
                               where r.IP_ADDRESS == ipAddress
                               select r).FirstOrDefault();

                    if (qry != null)
                    {
                        request.IpAddress = qry.ID;
                        //request.IPPort = ipPort;
                        request.IPPort = 80;
                        request.ServerName = serverName;
                    }
                    else
                    {
                        throw new Exception("You do not have access to this system.");
                    }
                }
                      

                BasicLookupResponse response = new BasicLookupResponse();

                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();

                IDictionary<string, object> input = new Dictionary<string, object>
                            {
                                {"CopQryRequest",request}
                            };

                IDictionary<string, object> output = WorkflowInvoker.Invoke(new Process_Values(), input);


                string InsureInfo = output["InsuranceInfo"].ToString();
                int IdNumber = (int)output["FieldID"];

                stopwatch.Stop();

                //Save milliseconds value to database
                IDictionary<string, object> inputTime = new Dictionary<string, object>
                            {
                                {"FieldId",IdNumber},
                                {"TimeInMS", stopwatch.ElapsedMilliseconds},
                                {"CopQryRequest",request}
                            };

                response.InsuranceInfo = InsureInfo;
                response.Result = common.CreateResult(ResultType.Success, "");



                IDictionary<string, object> outputTime = WorkflowInvoker.Invoke(new Save_Field_MS(), inputTime);

                //TODO What to do with the True or False return******************************************************************************
                bool saved = (bool)outputTime["SavedToDatabase"];
                

                return response;


            }
            catch (Exception err)
            {

                throw new Exception(err.Message);
            }


        }



        #endregion


    }
}
