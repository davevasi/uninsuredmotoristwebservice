﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Configuration; 

namespace UninsuredMotoristLIB.Common
{
    public class OverrideWCFAddressFilterServiceBehavior:IServiceBehavior
    {
        public void AddBindingParameters(ServiceDescription serviceDescription, System.ServiceModel.ServiceHostBase serviceHostBase, System.Collections.ObjectModel.Collection<ServiceEndpoint> endpoints, System.ServiceModel.Channels.BindingParameterCollection bindingParameters)
        {

        }

        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, System.ServiceModel.ServiceHostBase serviceHostBase)
        {
            for (int i = 0; i < serviceHostBase.ChannelDispatchers.Count; i++)
            {
                ChannelDispatcher channelDispatcher = serviceHostBase.ChannelDispatchers[i] as ChannelDispatcher;

                foreach (EndpointDispatcher dispatcher2 in channelDispatcher.Endpoints)
                {
                    dispatcher2.AddressFilter = new MatchAllMessageFilter();
                }
            }
        }

        public void Validate(ServiceDescription serviceDescription, System.ServiceModel.ServiceHostBase serviceHostBase)
        {

        }
    }
    public class OverrideAddressFilterModeElement : BehaviorExtensionElement
    {

        public override Type BehaviorType
        {
            get { return typeof(OverrideWCFAddressFilterServiceBehavior); }
        }

        protected override object CreateBehavior()
        {
            return new OverrideWCFAddressFilterServiceBehavior();
        } 

    }
}
