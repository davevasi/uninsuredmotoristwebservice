﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace UninsuredMotoristLIB.Common.Enums
{
    public enum ResultType
    {
        /// <summary>
        /// Successful Result.
        /// </summary>
        Success,
        /// <summary>
        /// Error Result.
        /// </summary>
        Error,
        /// <summary>
        /// Lead Data did not make it through Pre Qualification
        /// </summary>
        //InvalidLead,
        ///// <summary>
        ///// Something was not found .
        ///// </summary>
        //NotFound,
        ///// <summary>
        ///// Invalid parameter value supplied.
        ///// </summary>
        //InvalidInput,
        ///// <summary>
        ///// Authentication failed for some reason.
        ///// </summary>
        //ValidationError,
        ///// <summary>
        ///// Logon Error Occurred. 
        ///// </summary>
        //LogonError,
        ///// <summary>
        ///// Could Not Connect to Server
        ///// </summary>
        //ConnectionError,
        ///// <summary>
        ///// Image missing
        ///// </summary>
        //ImageMissingError,
        ///// <summary>
        ///// First Name Missing
        ///// </summary>
        //FirstNameMissingError,
        ///// <summary>
        ///// Last Name Missing
        ///// </summary>
        //LastNameMissingError,
        ///// <summary>
        ///// Street1 Missing
        ///// </summary>
        //Street1MissingError,
        ///// <summary>
        ///// City Missing
        ///// </summary>
        //CityMissingError,
        ///// <summary>
        ///// State Missing
        ///// </summary>
        //StateMissingError,
        ///// <summary>
        ///// ZipCode Missing
        ///// </summary>
        //ZipMissingError,
        ///// <summary>
        ///// Job Number Missing
        ///// </summary>
        //JobNumMissingError,
        ///// <summary>
        ///// Job Number is invalid
        ///// </summary>
        //InvalidJobNumber,
        ///// <summary>
        ///// Job Program is invalid
        ///// </summary>
        //JobProgramNotConfigured,
    }
}
