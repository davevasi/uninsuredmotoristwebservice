﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UninsuredMotoristLIB.Common.Enums;
using UninsuredMotoristLIB.Resource.Ent;

namespace UninsuredMotoristLIB.Common
{
    public static class common
    {
        public static ResultEnt CreateResult(ResultType resType, string Message)
        {
            ResultEnt ent = new ResultEnt();

            ent.ErrorCode = resType;
            ent.Message = Message;
            return ent;
        }
    }
}
