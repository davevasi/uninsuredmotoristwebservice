﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using UninsuredMotoristLIB.Resource.Ent;
using UninsuredMotoristLIB.Resource;


namespace UninsuredMotoristLIB
{
    [ServiceContract]
    public interface ISoapUninsuredMotoristService
    {
        [OperationContract]
        [FaultContract(typeof(ResultEnt))]
        BasicLookupResponse BasicLookupByType(BasicLookupRequest request);


       
    }
}
