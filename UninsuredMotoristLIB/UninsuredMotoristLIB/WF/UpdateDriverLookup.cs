﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;
using UninsuredMotoristLIB.Resource;

namespace UninsuredMotoristLIB.WF
{

    public sealed class UpdateDriverLookup : CodeActivity
    {
        // Define an activity input argument of type string
        public InArgument<int> fieldid { get; set; }
        public InArgument<decimal> millSec { get; set; }
        public OutArgument<Boolean> SavedToDB { get; set; }

        // If your activity returns a value, derive from CodeActivity<TResult>
        // and return the value from the Execute method.
        protected override void Execute(CodeActivityContext context)
        {
            bool successfullySaved = false;

            int myID = context.GetValue(fieldid);
            decimal myMS = context.GetValue(millSec);

            // Obtain the runtime value of the Text input argument

            try
            {
                Driver_Lookup dl = new Driver_Lookup();

                using (CopQueryDataContext dc = new CopQueryDataContext())
                {
                    var qry = dc.Driver_Lookups
                        .Where(w => w.Id == myID)
                        .SingleOrDefault();

                    if (qry != null)
                    {
                        qry.MSeconds = myMS;
                        dc.SubmitChanges();
                        successfullySaved = true;
                    }

                    dc.SubmitChanges();

                }

                SavedToDB.Set(context, successfullySaved);
            }
            catch (Exception err)
            {

                throw new Exception(err.Message);
            }

        }
    }
}
