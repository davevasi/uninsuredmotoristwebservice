﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;
using UninsuredMotoristLIB.Resource;

namespace UninsuredMotoristLIB.WF
{

    public sealed class ValidateLicensePlatecs : CodeActivity
    {
        // Define an activity input argument of type string
        public InArgument<BasicLookupRequest> requestVal { get; set; }
        public OutArgument<Boolean> lsResult { get; set; }

        // If your activity returns a value, derive from CodeActivity<TResult>
        // and return the value from the Execute method.
        protected override void Execute(CodeActivityContext context)
        {
            BasicLookupRequest myRequest = context.GetValue(requestVal);
            // Obtain the runtime value of the Text input argument

            try
            {
                if (myRequest.LicensePlate.Length > 7)
                {
                    lsResult.Set(context, false);
                }
                else
                {
                    lsResult.Set(context, true);
                }

            }
            catch (Exception err)
            {

                throw new Exception(err.Message);
            }

        }
    }
}
