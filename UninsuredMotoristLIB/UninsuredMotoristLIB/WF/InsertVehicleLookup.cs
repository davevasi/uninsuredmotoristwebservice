﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;
using UninsuredMotoristLIB.Resource;

namespace UninsuredMotoristLIB.WF
{

    public sealed class InsertVehicleLookup : CodeActivity
    {
        // Define an activity input argument of type string
        public InArgument<BasicLookupRequest> requestVal { get; set; }
        public InArgument<string> ReturnedVal { get; set; }
        public OutArgument<int> VehicleLookup { get; set; }

        // If your activity returns a value, derive from CodeActivity<TResult>
        // and return the value from the Execute method.
        protected override void Execute(CodeActivityContext context)
        {
            BasicLookupRequest myRequest = context.GetValue(requestVal);
            string myReturnVal = context.GetValue(ReturnedVal);
            // Obtain the runtime value of the Text input argument

            try
            {
                Vehicle_Lookup dl = new Vehicle_Lookup();

                using (CopQueryDataContext dc = new CopQueryDataContext())
                {

                    dl.VIN = myRequest.VIN;
                    dl.Plate = myRequest.LicensePlate;
                    dl.Date_l = DateTime.Now;
                    dl.Retval = myReturnVal;
                    dl.Source = myRequest.IpAddress;
                    dl.Server = myRequest.ServerName;
                    dl.Port = myRequest.IPPort;

                    dc.Vehicle_Lookups.InsertOnSubmit(dl);
                    dc.SubmitChanges();

                }

                VehicleLookup.Set(context, dl.ID);
            }
            catch (Exception err)
            {

                throw new Exception(err.Message);
            }

        }
    }
}
