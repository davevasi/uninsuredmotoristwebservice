﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;
using UninsuredMotoristLIB.Resource;

namespace UninsuredMotoristLIB.WF
{

    public sealed class InsertDriverLookup : CodeActivity
    {
        // Define an activity input argument of type string
        public InArgument<BasicLookupRequest> requestVal { get; set; }
        public InArgument<string> ReturnedVal { get; set; }
        public OutArgument<int> DriverLookup { get; set; }

        // If your activity returns a value, derive from CodeActivity<TResult>
        // and return the value from the Execute method.
        protected override void Execute(CodeActivityContext context)
        {
            BasicLookupRequest myRequest = context.GetValue(requestVal);
            string myReturnVal = context.GetValue(ReturnedVal);

            // Obtain the runtime value of the Text input argument

            try
            {
                Driver_Lookup dl = new Driver_Lookup();

                using (CopQueryDataContext dc = new CopQueryDataContext())
                {

                    dl.DL_Num = myRequest.DriversLicenseNumber.ToString();
                    dl.Date_L = DateTime.Now;
                    dl.RetVal = myReturnVal;
                    dl.Source = myRequest.IpAddress;
                    dl.Server = myRequest.ServerName;
                    dl.port = myRequest.IPPort;

                    dc.Driver_Lookups.InsertOnSubmit(dl);
                    dc.SubmitChanges();
                    
                }

                DriverLookup.Set(context, dl.Id);
            }
            catch (Exception err)
            {

                throw new Exception(err.Message);
            }

        }
    }
}
