﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;
using UninsuredMotoristLIB.Resource;

namespace UninsuredMotoristLIB.WF
{
    public sealed class GetInsuranceInfoDriver : CodeActivity
    {
        // Define an activity input argument of type string
        public InArgument<BasicLookupRequest> requestVal { get; set; }

        public OutArgument<String> InsuranceInformation { get; set; }

        // If your activity returns a value, derive from CodeActivity<TResult>
        // and return the value from the Execute method.
        protected override void Execute(CodeActivityContext context)
        { 
            try
            {
                BasicLookupRequest myRequest = context.GetValue(requestVal);
                // Obtain the runtime value of the Text input argument
                string converted = myRequest.DriversLicenseNumber;
          
                using (CopQueryDataContext dc = new CopQueryDataContext())
                {
                    var qry = (from t in dc.sproc_Find_Driver_Insurance(Int32.Parse(converted),null)
                               select t).SingleOrDefault();

                    if (qry.Result != "0")
                    {
                        InsuranceInformation.Set(context, qry.Result);
                    }
                    //else
                    //{
                    //    InsuranceInformation.Set(context, qry.Result);
                    //}
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}