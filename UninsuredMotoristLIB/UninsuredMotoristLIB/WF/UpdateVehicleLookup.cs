﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;
using UninsuredMotoristLIB.Resource;

namespace UninsuredMotoristLIB.WF
{

    public sealed class UpdateVehicleLookup : CodeActivity
    {
        public InArgument<int> fieldid { get; set; }
        public InArgument<decimal> millSec { get; set; }
        public OutArgument<Boolean> SavedToDB { get; set; }

        // If your activity returns a value, derive from CodeActivity<TResult>
        // and return the value from the Execute method.
        protected override void Execute(CodeActivityContext context)
        {
            bool successfullySaved = false;

            int myID = context.GetValue(fieldid);
            decimal myMS = context.GetValue(millSec);

            // Obtain the runtime value of the Text input argument

            try
            {
                Vehicle_Lookup dl = new Vehicle_Lookup();

                using (CopQueryDataContext dc = new CopQueryDataContext())
                {
                    var qry = dc.Vehicle_Lookups
                        .Where(w => w.ID == myID)
                        .SingleOrDefault();

                    if (qry != null)
                    {
                        qry.mseconds = myMS;
                        dc.SubmitChanges();
                        successfullySaved = true;
                    }

                    dc.SubmitChanges();

                }

                SavedToDB.Set(context, successfullySaved);
            }
            catch (Exception err)
            {

                throw new Exception(err.Message);
            }

        }
    }
}
