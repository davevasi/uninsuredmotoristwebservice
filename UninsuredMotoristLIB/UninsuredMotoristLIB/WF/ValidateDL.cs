﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;
using UninsuredMotoristLIB.Resource;
using System.Configuration;

namespace UninsuredMotoristLIB.WF
{

    public sealed class ValidateDL : CodeActivity
    {
        // Define an activity input argument of type string
        public InArgument<BasicLookupRequest> requestVal { get; set; }
        public OutArgument<Boolean> dlResult { get; set; }

        // If your activity returns a value, derive from CodeActivity<TResult>
        // and return the value from the Execute method.
        protected override void Execute(CodeActivityContext context)
        {
            BasicLookupRequest myRequest = context.GetValue(requestVal);
            // Obtain the runtime value of the Text input argument

            try
            {
                if (ConfigurationManager.AppSettings["NumericValidation"]=="true")
                {
                    int number;
                    bool result = int.TryParse(myRequest.DriversLicenseNumber, out number);
                    if (!result)
                    {
                        dlResult.Set(context, false);
                    }
                    else
                    {

                        using (CopQueryDataContext dc = new CopQueryDataContext())
                        {
                            var qry = (from t in dc.Invalid_DLs
                                       where t.DL_Num == myRequest.DriversLicenseNumber.ToString()
                                       select t).Count();

                            if (qry > 0)
                            {
                                dlResult.Set(context, false);
                            }
                            else
                            {
                                //check length and numerical only I think
                                dlResult.Set(context, true);
                            }
                        }
                    }

                }
                else
                {
                    dlResult.Set(context, false);
                }
            }
            catch (Exception err)
            {

                throw new Exception(err.Message);
            }

        }
    }
}
