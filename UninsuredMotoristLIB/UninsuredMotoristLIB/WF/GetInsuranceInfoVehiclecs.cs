﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;
using UninsuredMotoristLIB.Resource;

namespace UninsuredMotoristLIB.WF
{

    public sealed class GetInsuranceInfoVehiclecs : CodeActivity
    {
        // Define an activity input argument of type string
        public InArgument<BasicLookupRequest> requestVal { get; set; }
        public OutArgument<String> InsuranceInformation { get; set; }

        // If your activity returns a value, derive from CodeActivity<TResult>
        // and return the value from the Execute method.
        protected override void Execute(CodeActivityContext context)
        {
            BasicLookupRequest myRequest = context.GetValue(requestVal);
            // Obtain the runtime value of the Text input argument

            try
            {
                using (CopQueryDataContext dc = new CopQueryDataContext())
                {
                    var qry = (from t in dc.sproc_Find_Vehicle_Insurance(myRequest.LicensePlate, myRequest.VIN, null)
                               select t).SingleOrDefault();

                    if (qry.Result != String.Empty)
                    {
                        InsuranceInformation.Set(context, qry.Result);
                    }
                    else
                    {
                        //TODO Check to see what to do if Insurance Info is blank
                        InsuranceInformation.Set(context, qry.Result);

                    }
                }

            }
            catch (Exception err)
            {

                throw new Exception(err.Message);
            }

        }
    }
}
