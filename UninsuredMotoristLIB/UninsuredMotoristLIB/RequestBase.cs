﻿
namespace UninsuredMotoristLIB
{
    public abstract class RequestBase
    {
        private string sessionToken;

        /// <summary>
        /// Gets or sets the session token.
        /// </summary>
        /// <value>The session token.</value>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string SessionToken
        {
            get { return sessionToken; }
            set { sessionToken = value; }
        }
    }
}
